import * as xpath from "./xpath"
import * as variables from "./variables"

export {
    xpath,
    variables
}