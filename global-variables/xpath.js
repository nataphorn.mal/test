let xpathEmail = '//*[@id="email-input"]'
let xpathPassword = '//input[@name="password"]'
let xpathButtonLogin = '//*[@id="login-button"]'
let xpathSelectTeam = '(//*[@class = "team-name"]/parent::div)[1]'

export {
    xpathEmail,
    xpathPassword,
    xpathButtonLogin,
    xpathSelectTeam
}