import { login } from '../../../global-function'

describe('delete', () => {
    let xpathRowMenu = '//*[@class="box-s menu"]'
    let xpathMenu = '//*[@id="table-menu-'
    let xpathRemove = '//*[@id="table-remove-edit-'

    let xpathMainPage = '//*[@id="content-main-page"]'
    let xpathCheckbox = '//*[@type = "checkbox"]'
    let xpathRemoveButton = '//*[@id = "remove-order-btn"]'
    let xpathInputTextToConfirmDelete = '//*[@class = "form-control form-search ng-untouched ng-pristine ng-valid"]'
    let xpathConfirmButton = '//*[@id = "confirm-submit-button"]'
    let xpathNotify = '//*[@id="global-notifier"]'
    let xpathCancelButton = '//*[@id = "confirm-cancel-button"]'

    beforeEach(() => {
        login()
        cy.xpath(xpathMainPage).scrollTo('bottom')
        cy.wait(2000)
      });

    it('delete-001', () => {
        cy.xpath(xpathMainPage).scrollTo('bottom')
        cy.xpath('(' + xpathCheckbox + ')' + '[' + 2 + ']').click()
        cy.xpath(xpathRemoveButton).click()
        cy.wait(1000)
        cy.xpath(xpathInputTextToConfirmDelete).type('ลบ')
        cy.wait(1000)
        cy.xpath(xpathConfirmButton).click()
        cy.wait(1000)
        cy.xpath(xpathNotify).contains('ลบสำเร็จ 1 รายการ')
        cy.xpath(xpathMainPage).scrollTo('bottom')
    });

    it('delete-002', () => {
        cy.xpath(xpathCheckbox).then((length) => {
            const count = Cypress.$(length).length-1
            cy.log(count)
            cy.xpath('(' + xpathCheckbox + ')' + '[' + 1 + ']').click()
            cy.xpath(xpathRemoveButton).click()
            cy.wait(1000)
            cy.xpath(xpathInputTextToConfirmDelete).type('ลบ')
            cy.wait(1000)
            cy.xpath(xpathConfirmButton).click()
            cy.wait(1000)
            cy.xpath(xpathNotify).contains('ลบสำเร็จ '+ count +' รายการ')
            cy.xpath(xpathMainPage).scrollTo('bottom')
        })
    });

    it('delete-003', () => {
        cy.xpath(xpathRowMenu).then((length) => {
            const count = Cypress.$(length).length - 1
            cy.xpath(xpathMainPage).scrollTo('bottom')
            cy.xpath(xpathMenu + count + '"' + ']').click()
            cy.xpath(xpathRemove + count + '"' + ']').click()
            cy.wait(1000)
            cy.xpath(xpathInputTextToConfirmDelete).type('ลบ')
            cy.wait(1000)
            cy.xpath(xpathConfirmButton).click()
            cy.wait(1000)
            cy.xpath(xpathNotify).contains('ลบสำเร็จ 1 รายการ')
            cy.xpath(xpathMainPage).scrollTo('bottom')
            cy.xpath(xpathMenu + count + '"' + ']').should('not.to.exist')
        })
    });

    it('delete-004', () => {
        cy.xpath(xpathCheckbox).then((length) => {
            const count = Cypress.$(length).length
            cy.log(count)
            cy.xpath(xpathMainPage).scrollTo('bottom')
            cy.xpath('(' + xpathCheckbox + ')' + '[' + count + ']').click()
            cy.xpath(xpathRemoveButton).click()
            cy.wait(1000)
            cy.xpath(xpathInputTextToConfirmDelete).type('ลบ')
            cy.wait(1000)
            cy.xpath(xpathCancelButton).click()
            cy.xpath('(' + xpathCheckbox + ')' + '[' + count + ']').should('exist')
        })
    });

    it('delete-005', () => {
        cy.xpath(xpathRowMenu).then((length) => {
            const count = Cypress.$(length).length - 1
            cy.xpath(xpathMainPage).scrollTo('bottom')
            cy.xpath(xpathMenu + count + '"' + ']').click()
            cy.xpath(xpathRemove + count + '"' + ']').click()
            cy.wait(1000)
            cy.xpath(xpathInputTextToConfirmDelete).type('ลบ')
            cy.wait(1000)
            cy.xpath(xpathCancelButton).click()
            cy.xpath(xpathMenu + count + '"' + ']').should('exist')
        })
    });
});