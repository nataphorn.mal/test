import { login } from '../../../global-function'

describe('create order', () => {

  // valiable
  let address = 'ทดสอบ ระบบ\n0987654321\n69/429 หมู่ 2 ต.วิชิต อ.เมือง จ.ภูเก็ต 83000'
  let error = 'กรุณาเลือกช่องทางการขาย'
  let alertSaveSuccess = 'บันทึกสำเร็จ'
  let alertUploadFileSuccess = 'อัปโหลดไฟล์สำเร็จ'
  let alertNotInputCustomerInformation = 'กรุณาระบุข้อมูลผู้รับให้ครบถ้วน'
  let alertNotInputProductInformation = 'กรุณากรอกข้อมูลสินค้า'
  let alertNotSelectChannel = 'กรุณาเลือกช่องทางการขาย'
  let alertRepeatSlip = 'ไม่สามารถอัปโหลดได้ เนื่องจากพบข้อมูลสลิปซ้ำ กรุณากรอกข้อมูลการชำระเงินใหม่'

  let pleaseFillName = 'กรุณากรอก ชื่อ-นามสกุล ของผู้รับ'
  let pleaseFillTel = 'กรุณากรอกเบอร์โทรให้ครบถ้วน'
  let pleaseFillAddress = 'กรุณากรอกที่อยู่'
  let pleaseFillPosition = 'กรุณากรอกตำแหน่งของผู้รับ'
  let nameMoreThan50 = 'ไม่สามารถใช้ชื่อนี้ได้ เนื่องจากเกินความยาวที่กำหนด'
  let invalidTel = 'หมายเลขไม่ถูกต้อง'
  let pleaseFillIn10Digits = 'กรุณากรอกให้ครบ 10 หลัก'
  let seller = 'ณัฐพร มลิวัลย์'
  // xpath
  let xpathLine = '//*[@class="img-channel-icon line"]'
  let xpathInputDetectAddress = '//*[@id="address-detect-input"]'
  let xpathAddProduct = '//*[@id="custom-product-add"]'
  let xpathTitleAddProduct = '(//*[@class = "title row d-flex align-items-center m-0"]/parent::div)[2]'
  let xpathInputProductName = '//*[@id="custom-product-name-input-0"]'
  let xpathInputProductPrice = '//*[@id="custom-product-price-input-0"]'
  let xpathInputProductWeight = '//*[@id="custom-product-weight-input-0"]'
  let xpathSelectProductButton_1 = '(//*[@id = "select-product-button"])[1]'
  let xpathSelectPaymentGatway = '(//*[@id="dropdownMenuButton"])[2]'
  let xpathBank = '//*[@id="bank"]'
  let xpathCOD = '//*[@id="cod"]'
  let xpathSelectImage = '//*[@class = "input-card-area ng-star-inserted"]'
  let xpathCreateOrderButton = '//*[@class="confirm-button"]'
  let xpathCancelCreateOrderButton = '//*[@class = "cancel-button"]'
  let xpathTable = '//*[@class = "order-row ng-star-inserted"]'
  let xpathTableName = '//*[@id = "table-name-label-'
  let xpathTableOrder = '//*[@id = "table-order-label-'
  let xpathTableTel = '//*[@id = "table-tel-label-'
  let xpathTableProduct = '//*[@id = "table-product-label-'
  let xpathTableBalanceType = '//*[@id = "table-balance-type-label-'
  let xpathTableTextBank = '//*[@id = "table-text-bank-'
  let xpathTableChannel = '//span/parent::td[@id = "table-channel-'
  let xpathTableSeller = '//p[text() = "' + seller + '"]/parent::td'
  let xpathTableMenu = '//*[@id = "table-name-label-'
  let xpathOpenDownloadButton = '(//*[@id="open-download-button"])[2]'
  let xpathMyOrderButton = '//*[@id="myorder"]'
  let xpathInputFile = '//*[@class="input-upload-area"]'
  let xpathSubmitUploadButton = '//*[@class="submit-upload"]'
  let xpathNotify = '//*[@id="global-notifier"]'
  let xpathInvalidCustomerInformation = '//*[@class = "invalid-message ng-star-inserted"]'
  let xpathMainPage = '//*[@id="content-main-page"]'

  let xpathInputName = '//*[@id="customer-name-input"]'
  let xpathInputTel = '//*[@id="tel-input"]'
  let xpathInputAddress = '//*[@id="address-input"]'
  let xpathInputPosition = '//*[@id="search-address"]'
  let xpathDeleteCustomerInfomationButton = '//*[@id="clear-address-button"]'

  let xpartSum = '//*[@class="form-control bg-white summary-text"]'
  let xpathEditProduct = '//*[@id="edit-product-in-basket-button"]'
  let xpathTrashButton = '(//*[@class="trash"])[2]'

  let xpathConfirmButton = '//*[@id="confirm-submit-button"]'
  let xpathSelectProductButton_2 = '(//*[@id="select-product-button"])[2]'
  let xpathProductName = '//*[@id="product-name-"]'
  let xpathProductWeight = '//*[@class="input-group-weight mb-0"]'
  let xpathIncreaseButton = '(//*[@id="increase-amount-0"])[2]'
  let xpathDecreaseButton = '//*[@id="decrease-amount-0"]'
  let xpathRecentProduct = '//*[@id = "recent-custom-product"]'
  let xpathInputShippingCost = '(//*[@class="form-control ng-untouched ng-valid ng-dirty"])[1]'
  let xpathInputDiscount = '(//*[@class="form-control ng-untouched ng-valid ng-dirty"])[2]'

  let xpathInputAmount = '//*[@id="amount-input"]'
  let xpathInputDate = '//*[@id="date-input"]'
  let xpathInputTime = '//*[@id="time-input"]'
  let xpathInvalidDateAndTime = '(//*[@class="col-12"]/p)[2]'
  let xpathInvalidAmount = '//*[@class="col-12 mb-2"]/p'
  let xpathInputRecipient = '//*[@id="receive-payment-input"]'

  beforeEach(() => {
    login()
  });

  function addProduct() {
    cy.xpath(xpathAddProduct).click()
    cy.xpath(xpathTitleAddProduct).contains('เพิ่มสินค้า')
    cy.xpath(xpathInputProductName).type('สบู่')
    cy.wait(1000)
    cy.xpath(xpathInputProductPrice).type('20')
    cy.wait(1000)
    cy.xpath(xpathInputProductWeight).type('5')
    cy.xpath(xpathSelectProductButton_1).click()
  }

  function bank() {
    cy.xpath(xpathSelectPaymentGatway).click()
    cy.wait(2000)
    cy.xpath(xpathBank).click()
    cy.wait(3000)
    cy.xpath(xpathSelectImage).selectFile('cypress/fixtures/slip.JPG', { action: 'drag-drop' })
  }

  function cod() {
    cy.xpath(xpathSelectPaymentGatway).click()
    cy.wait(2000)
    cy.xpath(xpathCOD).click()
  }

  it('create-001', () => {
    cy.xpath(xpathLine).click()
    cy.wait(2000)
    cy.xpath(xpathInputDetectAddress).type(address)
    addProduct()
    cy.wait(2000)
    bank()
    cy.wait(1000)
    cy.xpath(xpathCreateOrderButton).click()
    cy.xpath(xpathNotify).contains(alertSaveSuccess).should('be.visible')
    cy.xpath(xpathMainPage).scrollTo('bottom')
    cy.wait(1000)
    cy.xpath(xpathTableName+0+'"'+']').contains(' ทดสอบ ระบบ ').should('be.visible')
    cy.xpath(xpathTableTel+0+'"'+']').contains('098-765-4321').should('be.visible')
    cy.xpath(xpathTableProduct+0+'"'+']').contains('สบู่(1)').should('be.visible')
    cy.xpath(xpathTableBalanceType+0+'"'+']').contains(' 20 (BANK)').should('be.visible')
    cy.xpath(xpathTableTextBank+0+'"'+']').contains(' 27/11/2023 09:49 ').should('be.visible')
    cy.xpath(xpathTableChannel+0+'"'+']').contains('Line').should('be.visible')
    cy.xpath(xpathTableSeller).contains('ณัฐพร มลิวัลย์').should('be.visible')
  })

  it('create-002', () => {
    cy.xpath(xpathOpenDownloadButton).click()
    cy.xpath(xpathMyOrderButton).click()
    cy.xpath(xpathInputFile).selectFile('cypress/fixtures/MyOrder_Default_Template (1).xlsx', { action: 'drag-drop' })
    cy.xpath(xpathSubmitUploadButton).click()
    cy.xpath(xpathNotify).contains(alertUploadFileSuccess).should('be.visible')
    cy.wait(2000)
    cy.xpath(xpathTable).then((row) => {
      const rowCount = Cypress.$(row).length
      cy.log(rowCount)
      for (let i = 0; i <= 3; i++) {
        const tableSeller = i + 1
        if (i === 0) {
          cy.xpath(xpathTableName + i + '"' + ']').contains(' เทส ').should('exist')
          cy.xpath(xpathTableTel + i + '"' + ']').contains('091-234-5678').should('be.visible')
          cy.xpath(xpathTableProduct + i + '"' + ']').contains('ขนม(1)').should('be.visible')
          cy.xpath(xpathTableBalanceType + i + '"' + ']').contains(' 100 (BANK)').should('be.visible')
          cy.xpath(xpathTableTextBank + i + '"' + ']').contains(' 29/09/2021 14:00 ').should('be.visible')
          cy.xpath(xpathTableChannel + i + '"' + ']').contains('Tiktok').should('be.visible')
          cy.xpath('(' + xpathTableSeller + ')' + '[' + tableSeller + ']').contains('ณัฐพร มลิวัลย์').should('be.visible')
        } else if (i === 1) {
          cy.xpath(xpathTableName + i + '"' + ']').contains(' ตัวอย่าง ').should('be.visible')
          cy.xpath(xpathTableTel + i + '"' + ']').contains('091-234-5678').should('be.visible')
          cy.xpath(xpathTableProduct + i + '"' + ']').contains('ขนม(1)').should('be.visible')
          cy.xpath(xpathTableBalanceType + i + '"' + ']').contains(' 100 (BANK)').should('be.visible')
          cy.xpath(xpathTableTextBank + i + '"' + ']').contains(' 29/09/2021 14:00 ').should('be.visible')
          cy.xpath(xpathTableChannel + i + '"' + ']').contains('Instagram').should('be.visible')
          cy.xpath('(' + xpathTableSeller + ')' + '[' + tableSeller + ']').contains('ณัฐพร มลิวัลย์').should('be.visible')
        } else if (i === 2) {
          cy.xpath(xpathTableName + i + '"' + ']').contains(' ทดสอบ ').should('be.visible')
          cy.xpath(xpathTableTel + i + '"' + ']').contains('091-234-5678').should('be.visible')
          cy.xpath(xpathTableProduct + i + '"' + ']').contains('ขนม(1)').should('be.visible')
          cy.xpath(xpathTableBalanceType + i + '"' + ']').contains(' 100.12 (BANK)').should('be.visible')
          cy.xpath(xpathTableTextBank + i + '"' + ']').contains(' 29/09/2021 13:00 ').should('be.visible')
          cy.xpath(xpathTableChannel + i + '"' + ']').contains('Line').should('be.visible')
          cy.xpath('(' + xpathTableSeller + ')' + '[' + tableSeller + ']').contains('ณัฐพร มลิวัลย์').should('be.visible')
        }
      }
    }
    )
  });

  it('create-003', () => {
    cy.xpath(xpathLine).click()
    cy.wait(2000)
    addProduct()
    cy.wait(2000)
    cod()
    cy.wait(1000)
    cy.xpath(xpathCreateOrderButton).click()
    cy.xpath(xpathNotify).contains(alertNotInputCustomerInformation).should('be.visible')
    cy.wait(1000)
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.wait(2000)
    cy.xpath(xpathInvalidCustomerInformation).then((length) => {
      const count = Cypress.$(length).length
      for (let i = 1; i <= count; i++) {
        if (i === 1) {
          cy.xpath('('+xpathInvalidCustomerInformation+')'+'['+i+']').scrollIntoView().contains(pleaseFillName).should('be.visible')
        } else if (i === 2) {
          cy.xpath('('+xpathInvalidCustomerInformation+')'+'['+i+']').contains(pleaseFillTel).should('be.visible')
        } else if (i === 3) {
          cy.xpath('('+xpathInvalidCustomerInformation+')'+'['+i+']').contains(pleaseFillAddress).should('be.visible')
        } else if (i === 4) {
          cy.xpath('('+xpathInvalidCustomerInformation+')'+'['+i+']').contains(pleaseFillPosition).should('be.visible')
        }
      }
    })
  });

  it('create-004', () => {
    cy.xpath(xpathLine).click()
    cy.wait(2000)
    cy.xpath(xpathInputDetectAddress).type(address)
    cod()
    cy.wait(1000)
    cy.xpath(xpathCreateOrderButton).click()
    cy.xpath(xpathNotify).contains(alertNotInputProductInformation).should('be.visible')
  });

  it('create-005', () => {
    cy.xpath(xpathLine).click()
    cy.wait(2000)
    cy.xpath(xpathInputDetectAddress).type(address)
    addProduct()
    cy.wait(2000)
    bank()
    cy.xpath(xpathNotify).contains(alertRepeatSlip).should('be.visible')
  });  

  it('create-006', () => {
    cy.xpath(xpathLine).click()
    cy.wait(2000)
    cy.xpath(xpathInputDetectAddress).type(address)
    addProduct()
    cy.wait(2000)
    cod()
    cy.wait(1000)
    cy.xpath(xpathCancelCreateOrderButton).click()
    cy.contains(alertSaveSuccess).should('not.exist')
  });

  it('create-007', () => {
    cy.xpath(xpathInputDetectAddress).type(address)
    addProduct()
    cy.wait(2000)
    cod()
    cy.wait(1000)
    cy.xpath(xpathCreateOrderButton).click()
    cy.xpath(xpathNotify).contains(alertNotSelectChannel).should('be.visible')
  });

  it('create-008', () => {
    cy.xpath(xpathInputDetectAddress).type(address)
    cy.xpath(xpathInputName).should('have.value', 'ทดสอบ ระบบ')
    cy.xpath(xpathInputTel).should('have.value', '098-765-4321')
    cy.xpath(xpathInputAddress).should('have.value', '69/429 หมู่ 2')
    cy.xpath(xpathInputPosition).should('have.value', 'ตำบลวิชิต อำเภอเมืองภูเก็ต จังหวัดภูเก็ต 83000')
  });

  it('create-009', () => {
    cy.xpath(xpathInputDetectAddress).type(address)
    cy.wait(1000)
    cy.xpath(xpathDeleteCustomerInfomationButton).click()
    cy.wait(1000)
    cy.xpath(xpathInputName).should('have.value', '')
    cy.xpath(xpathInputTel).should('have.value', '')
    cy.xpath(xpathInputAddress).should('have.value', '')
    cy.xpath(xpathInputPosition).should('have.value', '')
  });

  it('create-010', () => {
    cy.xpath(xpathLine).click()
    cy.xpath(xpathCreateOrderButton).click()
    cy.xpath(xpathNotify).contains(alertNotInputCustomerInformation).should('be.visible')
    cy.wait(1000)
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.wait(2000)
    cy.xpath(xpathInvalidCustomerInformation).then((length) => {
      const count = Cypress.$(length).length
      for (let i = 1; i <= count; i++) {
        if (i === 1) {
          cy.xpath('(' + xpathInvalidCustomerInformation + ')' + '[' + i + ']').scrollIntoView().contains(pleaseFillName).should('be.visible')
        } else if (i === 2) {
          cy.xpath('(' + xpathInvalidCustomerInformation + ')' + '[' + i + ']').contains(pleaseFillTel).should('be.visible')
        } else if (i === 3) {
          cy.xpath('(' + xpathInvalidCustomerInformation + ')' + '[' + i + ']').contains(pleaseFillAddress).should('be.visible')
        } else if (i === 4) {
          cy.xpath('(' + xpathInvalidCustomerInformation + ')' + '[' + i + ']').contains(pleaseFillPosition).should('be.visible')
        }
      }
    })
  });

  it('create-011', () => {
    cy.xpath(xpathInputName).type('ทดสอบ การกรอกชื่อ-นามสกุล ที่มีมากกว่า 50 ตัวอักษรร')
    cy.wait(1000)
    cy.xpath('(' + xpathInvalidCustomerInformation + ')' + '[' + 1 + ']').contains(nameMoreThan50).should('be.visible')
  });

  it('create-012', () => {
    cy.xpath(xpathInputTel).type('ทดสอบ')
    cy.xpath(xpathInputTel).should('have.value', '')
  });

  it('create-013', () => {
    cy.xpath(xpathInputTel).type('129-867-8888')
    cy.xpath('(' + xpathInvalidCustomerInformation + ')' + '[' + 1 + ']').contains(invalidTel).should('be.visible')
  });  

  it('create-014', () => {
    cy.xpath(xpathInputTel).type('098')
    cy.xpath('(' + xpathInvalidCustomerInformation + ')' + '[' + 1 + ']').contains(pleaseFillIn10Digits).should('be.visible')
  }); 

  it('create-016', () => {
    addProduct()
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.wait(1000)
    cy.xpath(xpartSum).should('have.value', '20')
  });

  it('create-017', () => {
    addProduct()
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.wait(1000)
    cy.xpath(xpathEditProduct).click()
    cy.wait(1000)
    cy.xpath(xpathTrashButton).click()
    cy.wait(1000)
    cy.xpath(xpathConfirmButton).click()
    cy.xpath(xpathSelectProductButton_2).click()
    cy.wait(1000)
    cy.xpath(xpathProductName).should('not.to.exist')
    cy.xpath(xpartSum).should('not.to.exist')
    cy.xpath(xpathProductWeight).should('not.to.exist')
  });

  it('create-018', () => {
    addProduct()
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.wait(1000)
    cy.xpath(xpartSum).should('have.value', '20')
    cy.xpath(xpathEditProduct).click()
    cy.wait(1000)
    cy.xpath(xpathIncreaseButton).click()
    cy.xpath(xpathSelectProductButton_2).click()
    cy.wait(1000)
    cy.xpath(xpartSum).should('have.value', '40')
    cy.xpath(xpathEditProduct).click()
    cy.wait(1000)
    cy.xpath(xpathDecreaseButton).click()
    cy.xpath(xpathSelectProductButton_2).click()
    cy.wait(1000)
    cy.xpath(xpartSum).should('have.value', '20')
  });

  it('create-019', () => {
    cy.xpath(xpathLine).click()
    cy.wait(2000)
    cy.xpath(xpathInputDetectAddress).type(address)
    addProduct()
    cy.wait(2000)
    cod()
    cy.wait(1000)
    cy.xpath(xpathCreateOrderButton).click()
    cy.xpath(xpathNotify).contains(alertSaveSuccess).should('be.visible')
    cy.wait(1000)
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.wait(1000)
    cy.xpath(xpathRecentProduct).click()
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.xpath(xpartSum).should('have.value', '20')
  });

  it('create-020', () => {
    addProduct()
    cy.xpath(xpathInputShippingCost).type('12')
    cy.xpath(xpathInputDiscount).type('5')
    cy.xpath(xpartSum).should('have.value', '27')
  });

  it('create-021', () => {
    cy.xpath(xpathLine).click()
    cy.wait(2000)
    cy.xpath(xpathInputDetectAddress).type(address)
    addProduct()
    cy.xpath(xpathInputAmount).clear()
    cy.wait(1000)
    cy.xpath(xpathInputDate).clear()
    cy.wait(1000)
    cy.xpath(xpathInputTime).clear()
    cy.xpath(xpathInvalidDateAndTime).contains('กรุณาเลือกวันที่และเวลาที่โอน').should('be.visible')
    cy.xpath(xpathCreateOrderButton).click()
    cy.xpath(xpathNotify).contains('กรุณาระบุข้อมูลการชำระเงินให้ครบถ้วน').should('be.visible')
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.xpath(xpathInvalidAmount).contains('กรุณากรอกจำนวนเงินที่โอน').should('be.visible')
  });

  it('create-022', () => {
    addProduct()
    cy.xpath(xpathInputAmount).clear()
    cy.xpath(xpathInputAmount).type('50')
    cy.xpath(xpathInputDate).clear()
    cy.xpath(xpathInputDate).type('2023-12-04')
    cy.xpath(xpathInputTime).clear()
    cy.xpath(xpathInputTime).type('01:58')
    cy.xpath(xpathMainPage).scrollTo('top')
    cy.xpath(xpathInputAmount).should('have.value', '50')
    cy.xpath(xpathInputDate).should('have.value', '2023-12-04')
    cy.xpath(xpathInputTime).should('have.value', '01:58')
  });

  it('create-023', () => {
    addProduct()
    cy.xpath(xpathInputAmount).should('have.value', '20')
  });

  it('create-024', () => {
    cy.xpath(xpathSelectImage).selectFile('cypress/fixtures/IMG_1993.JPG', { action: 'drag-drop' })
    cy.wait(1000)
    cy.xpath(xpathInputAmount).should('have.value', '1240')
    cy.xpath(xpathInputDate).should('have.value', '2023-12-02')
    cy.xpath(xpathInputTime).should('have.value', '18:27')
    cy.xpath(xpathInputRecipient).should('have.value', 'นาง ธัญญารัตน์ พ')
  });
})
