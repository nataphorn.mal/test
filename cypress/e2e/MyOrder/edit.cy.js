import { login } from '../../../global-function'

describe('edie order', () => {
    let seller = 'ณัฐพร มลิวัลย์'
    let alertSaveSuccess = 'บันทึกรายการสำเร็จแล้ว'

    let xpathMenu = '//*[@id="table-menu-0"]'
    let xpathEdit = '//*[@id="table-menu-edit-0"]'

    let xpathEditName = '(//*[@id="customer-name-input"])[2]'
    let xpathEditTel = '//*[@id="customer-tel-input"]'
    let xpathEditAddress = '//*[@id="customer-address-input"]'
    let xpathEditPosition = '//*[@id="customer-address"]'
    let xpathInsertPosition = '//*[@id="customer-address-search"]'
    let xpathSelectPosition = '//*[@class="tt-suggestion tt-selectable"]'
    let xpathInsertEmailAndCommentButton = '//*[@id="insert-email-comment"]'
    let xpathInputEmail = '(//*[@id="email-input"])[2]'
    let xpathInputComment = '(//*[@id="comment-input"])[2]'
    let xpathInputExpressPrice = '//*[@id="customer-express-price-input"]'
    let xpathInputDiscount = '//*[@id="customer-discount-input"]'

    let xpathSelectPaymentGatway = '(//*[@id="select-payment-type-input"])[2]'
    let xpathCOD = '(//*[@id="cod"])[2]'

    let xpathAddProduct = '//*[@id="insert-product-btn"]'
    let xpathTitleAddProduct = '(//*[@class = "title row d-flex align-items-center m-0"])[2]'

    let xpathInserProduceMyselfButton = '//*[@id="add-product-order-modal"]/div/div/div/div/app-product-menu/div/div[4]'
    let xpathInputProductName = '//*[@id="custom-product-name-input-0"]'
    let xpathInputProductPrice = '//*[@id="custom-price-input-0"]'
    let xpathInputProductWeight = '//*[@id="custom-weight-input-0"]'
    let xpathSelectProduct = '(//*[@id = "select-product-button"])[3]'
    let xpathIncrease = '//*[@id = "custom-plus-button-0"]'

    let xpathSubmitChangeButton = '//*[@id = "submit-change-order-btn"]'
    let xpathCancelChangeButton = '//*[@id = "cancel-change-order-btn"]'

    let xpathTableName = '//*[@id = "table-name-label-0"]'
    let xpathTableOrder = '//*[@id = "table-order-label-0"]'
    let xpathTableTel = '//*[@id = "table-tel-label-0"]'
    let xpathTableProduct = '//*[@id = "table-product-label-0"]'
    let xpathTableBalanceType = '//*[@id = "table-balance-type-label-0"]'
    let xpathTableTextBank = '//*[@id = "table-text-bank-0"]'
    let xpathTableTextCod = '//*[@id = "table-text-cod-0"]'
    let xpathTableChannel = '//span/parent::td[@id = "table-channel-0"]'
    let xpathTableSeller = '(//p[text() = "' + seller + '"]/parent::td)[1]'

    let xpathNotify = '//*[@id="global-notifier"]'
    let xpathConfirmToCancelChangeInformation = '//*[@id = "handle-exit-event"]'
    let xpathMainPage = '//*[@id="content-main-page"]'

    beforeEach(() => {
        login()
        cy.xpath(xpathMenu).click()
        cy.xpath(xpathEdit).click()
        cy.wait(2000)
      });

    function cod() {
        cy.xpath(xpathSelectPaymentGatway).click()
        cy.wait(2000)
        cy.xpath(xpathCOD).click()
    }

    function addProduct() {
        cy.xpath(xpathAddProduct).click()
        cy.xpath(xpathTitleAddProduct).contains('เพิ่มสินค้า')
        cy.wait(2000)
        cy.xpath(xpathInserProduceMyselfButton).click()
        cy.xpath(xpathInputProductName).type('สบู่')
        cy.wait(1000)
        cy.xpath(xpathInputProductPrice).type('20')
        cy.wait(1000)
        cy.xpath(xpathInputProductWeight).type('5')
        cy.xpath(xpathIncrease).click()
        cy.xpath(xpathSelectProduct).click()
    }


    it('edit-001', () => {
        cy.xpath(xpathEditName).clear()
        cy.xpath(xpathEditName).type('เปลี่ยนชื่อ ผู้รับ')
        cy.xpath(xpathEditTel).clear()
        cy.xpath(xpathEditTel).type('0986549875')
        cy.xpath(xpathEditAddress).clear()
        cy.xpath(xpathEditAddress).type('22/11 หมู่ 2')
        cy.xpath(xpathEditPosition).clear()
        cy.xpath(xpathInsertPosition).type('ต.ชุมพร อ.เมยวดี จ.ร้อยเอ็ด 45250')
        cy.xpath(xpathSelectPosition).click()
        cy.xpath(xpathInsertEmailAndCommentButton).click()
        cy.xpath(xpathInputEmail).type('1234@gmail.com')
        cy.xpath(xpathInputComment).type('แก้ไขที่อยู่')
        cy.xpath(xpathInputExpressPrice).clear()
        cy.xpath(xpathInputExpressPrice).type('20')
        cy.xpath(xpathInputDiscount).clear()
        cy.xpath(xpathInputDiscount).type('10')
        cod()
        addProduct()
        cy.wait(1000)
        cy.xpath(xpathSubmitChangeButton).click()
        cy.wait(1000)
        cy.xpath(xpathMainPage).scrollTo('bottom')
        cy.wait(1000)
        cy.xpath(xpathNotify).contains(alertSaveSuccess).should('be.visible')
        cy.xpath(xpathTableName).contains(' เปลี่ยนชื่อ ผู้รับ ').should('be.visible')
        cy.xpath(xpathTableTel).contains('098-654-9875').should('be.visible')
        cy.xpath(xpathTableProduct).contains('สบู่(2)').should('be.visible')
        cy.xpath(xpathTableBalanceType).contains(' 50 (COD)').should('be.visible')
        cy.xpath(xpathTableTextCod).contains('เก็บเงินปลายทาง').should('be.visible')
        cy.xpath(xpathTableChannel).contains('Tiktok').should('be.visible')
        cy.xpath( xpathTableSeller).contains('ณัฐพร มลิวัลย์').should('be.visible')
    });

    it('edit-002', () => {
        cy.xpath(xpathEditName).clear()
        cy.xpath(xpathEditName).type('ทดสอบ')
        cy.xpath(xpathEditTel).clear()
        cy.xpath(xpathEditTel).type('0564329876')
        cy.xpath(xpathEditAddress).clear()
        cy.xpath(xpathEditAddress).type('22/11')
        cy.xpath(xpathInputExpressPrice).clear()
        cy.xpath(xpathInputExpressPrice).type('30')
        cy.xpath(xpathInputDiscount).clear()
        cy.xpath(xpathInputDiscount).type('50')
        cod()
        addProduct()
        cy.wait(1000)
        cy.xpath(xpathCancelChangeButton).click()
        cy.xpath(xpathConfirmToCancelChangeInformation).click()
        cy.wait(1000)
        cy.xpath(xpathMainPage).scrollTo('bottom')
        cy.wait(1000)
        cy.xpath(xpathTableName).contains(' เปลี่ยนชื่อ ผู้รับ ').should('be.visible')
        cy.xpath(xpathTableTel).contains('098-654-9875').should('be.visible')
        cy.xpath(xpathTableProduct).contains('สบู่(2)').should('be.visible')
        cy.xpath(xpathTableBalanceType).contains(' 50 (COD)').should('be.visible')
        cy.xpath(xpathTableTextCod).contains('เก็บเงินปลายทาง').should('be.visible')
        cy.xpath(xpathTableChannel).contains('Tiktok').should('be.visible')
        cy.xpath( xpathTableSeller).contains('ณัฐพร มลิวัลย์').should('be.visible')
    });
});